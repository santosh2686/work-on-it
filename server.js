const path = require('path');
const express = require('express');
const compression = require('compression');
const app = express();
const port = process.env.PORT || 8080;

app.use(compression());
app.use(express.static(__dirname));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.listen(port, () => {
  console.log('Application Running on port: ' + port);
});
