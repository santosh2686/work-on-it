import update from 'immutability-helper';

import { RECENT_PROJECTS, ALL_PROJECTS, STATISTICS, IS_LOADING, SELECTED_PROJECT, FREELANCER_LIST } from '../actions';

const reducer = (state = {
  selectedProject: {}
}, action) => {
  switch (action.type) {
    case 'IS_LOADING': {
      return update(state, {
        $merge: {
          isLoading: action.flag
        }
      })
    }
    case 'RECENT_PROJECTS': {
      return update(state, {
        $merge: {
          'projects': action.res.data
        }
      });
    }
    case 'ALL_PROJECTS': {
      return update(state, {
        $merge: {
          'allProjects': action.res.data
        }
      });
    }
    case 'STATISTICS' : {
      return update(state, {
        $merge: {
          'statistics': action.res
        }
      });
    }
    case 'SELECTED_PROJECT': {
      return update(state, {
        selectedProject: {
          $merge: {
            [action.id]: action.res
          }
        }
      });
    }
    case 'FREELANCER_LIST': {
      return update(state, {
        $merge: {
          'freelancerList': action.res.data
        }
      });
    }
    default:
      return state;
  }
};

export default reducer;
