import React, {Component} from 'react';

class BidProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.minRate,
      duration: 1
    };
    super(props);
    this.changeHandler = this.changeHandler.bind(this);
    this.durationChangeHandler = this.durationChangeHandler.bind(this);
  }

  changeHandler({target: value}) {
    this.setState(value);
  }

  durationChangeHandler({target: {value}}) {
    this.setState({duration: value});
  }

  render() {
    const {lowestBid, status, minRate, maxRate} = this.props;
    return (
      <div>
        <div className="pad-tb-10">
          <div className="grid-row text-center">
            <div className="grid-block">
              <p className="color-gray">Set your rate</p>
              <div className="font-24 pad-b-15">${this.state.value}</div>
            </div>
            <div className="grid-block">
              <p className="color-gray">Lowest Bid</p>
              <div className="font-24 pad-b-15">${lowestBid}</div>
            </div>
            <div className="grid-block">
              <p className="color-gray pad-b-5">Project Status</p>
              <span className="show-inline-block bg-warning color-white pad-lr-10 days-left uppercase">{status}</span>
            </div>
          </div>
          <input value={this.state.value} onChange={this.changeHandler} type="range" min={minRate} max={maxRate} step="1"/>
        </div>
        <div className="pad-tb-10">
          <p className="color-gray">Set your delivery time</p>
          <div className="grid-row pad-t-5">
            <div className="grid-block input-group">
              <input type="text" className="input-control" value={this.state.duration} onChange={this.durationChangeHandler}/>
            </div>
            <div className="grid-block input-group">
              <div className="input-group pad-b-0 select-box relative">
                <select className="input-control">
                  <option>Days</option>
                  <option>Hours</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <button className="btn btn-primary btn-block">Place a Bid</button>
      </div>
    )
  }
}

export default BidProject;
