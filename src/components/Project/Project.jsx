import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux'
import {mapStateToProps, mapDispatchToProps} from './project.conf';
import Panel from '../Panel/Panel.jsx';
import Chip from '../Chip/Chip.jsx';
import BidProject from '../BidProject/BidProject.jsx';
import FreelancerList from '../FreelancerList/FreelancerList.jsx';
import Spinner from '../Spinner/Spinner.jsx';
import {daysLeft} from '../../utils';

class Project extends Component {
  componentDidMount() {
    this.props.getProject();
  }

  render() {
    const {isLoading, data} = this.props;
    if (isLoading || !data.id) {
      return <Spinner/>
    }

    return (<div>
      <div className="flex flex-space-between align-center flex-no-shrink flex-responsive">
        <h4>{data.title}
          <span className="font-12 show-inline-block bg-warning pad-lr-10 mar-l-10 color-white days-left">
              {daysLeft(data.date, data.lastDate)} Days Left
            </span>
        </h4>
        <div className="breadcrumb">
          <ol>
            <li><NavLink to="/dashboard">Dashboard</NavLink></li>
            <li><NavLink to="/projects">Project Search</NavLink></li>
            <li>Project Detail</li>
          </ol>
        </div>
      </div>
      <div className="grid-row">
        <div className="grid-block">
          <Panel title="Project Description">
            <div className="color-gray text-justify">
              <p className="pad-b-10">{data.description}</p>
              <p className="pad-b-10">{data.description2}</p>
              <p className="pad-b-10">{data.description3}</p>
            </div>

            <h4 className="pad-t-20 pad-b-10">Skills Required</h4>
            <div className="flex">
              {data.skills.map((skill, index) => {
                return (<Chip key={index} data={skill}/>);
              })}
            </div>

            <h4 className="pad-t-20">Attachments</h4>
            <div className="pad-t-10 flex">
              {data.attachments.map((item, index) => {
                return (<div key={index}
                             className="pad-20 text-center bg-gray-lighter mar-r-10 relative attachment cur-pointer">
                  <p className="pad-b-10 font-bold">{item.name}</p>
                  <span className="color-gray uppercase">{item.type}</span>
                </div>);
              })
              }
            </div>
          </Panel>
          <Panel title="Freelancers Bidding">
            <FreelancerList id={data.id}/>
          </Panel>
        </div>
        <div className="grid-block cell-1of3 flex flex-vertical">

          <Panel title="Project Budget">
            <div className="flex align-center">
              <span className="font-24 color-success">${data.minRate} - ${data.maxRate}</span>
              <span className="color-gray mar-l-10">{data.type} Rate</span>
            </div>
          </Panel>

          <Panel title="Other Details">
            <div className="flex">
              <div>
                <label className="show-block pad-b-15">Posted on: </label>
                <label className="show-block pad-b-15">Last day for Bid: </label>
                <label className="show-block">Number of Bids: </label>
              </div>
              <div className="color-gray pad-l-20">
                <span className="show-block pad-b-15"> {new Date(data.date).toLocaleString()} </span>
                <span className="show-block pad-b-15"> {new Date(data.lastDate).toLocaleString()} </span>
                <span className="show-block"> {data.noOfBids} </span>
              </div>
            </div>
          </Panel>

          <Panel title="Bid on this job!">
            <div className="pad-b-20">
              <BidProject lowestBid={data.lowestBid} status={data.status} minRate={data.minRate}
                          maxRate={data.maxRate}/>
            </div>
            <div className="pad-t-15 bor-t-gray-light text-center">
              <span>Don't have an account?</span>
              <a href="#"> Sign Up</a>
            </div>
          </Panel>
        </div>
      </div>
    </div>);
  }
}

Project.propTypes = {
  isLoading: PropTypes.bool,
  data: PropTypes.object,
  getProject: PropTypes.func
};
Project.defaultProps = {
  isLoading: true,
  data: {
    skills: [],
    attachments: [],
    getProject: () => {
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Project);
