import {isLoading, getProjectById} from '../../actions';

export const mapDispatchToProps = (dispatch, {match: {params: {id}}}) => {
  return {
    getProject: () => {
      dispatch(isLoading(true));
      setTimeout(() => {
        dispatch(getProjectById(id));
      }, 300);
    }
  }
};

export const mapStateToProps = (state, {match: {params: {id}}}) => {
  const {isLoading, selectedProject} = state;
  return {
    isLoading,
    data: selectedProject[id]
  }
};
