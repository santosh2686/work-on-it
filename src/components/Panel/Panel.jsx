import React from 'react';
import PropTypes from 'prop-types';

const Panel = ({ title, isRequired, children , extraClasses}) => {
  return (
    <div className={`panel bg-white pad-15 mar-t-20 ${extraClasses}`}>
      { title &&
        <div className="flex flex-space-between bor-b-gray-light pad-b-5 mar-b-15">
          <span className="font-16">{ title }</span>
          {isRequired && <span className="font-12 color-gray">* Required fields</span>}
          </div>
      }
      { children }
    </div>
  );
};

Panel.prototypes = {
  title: PropTypes.string,
  isRequired: PropTypes.bool,
  extraClasses: PropTypes.string,
};

Panel.defaultProps = {
  title: '',
  isRequired: false,
  extraClasses: ''
};

export default Panel;
