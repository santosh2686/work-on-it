import { getAllProjects, isLoading } from '../../actions';

export const mapDispatchToProps = (dispatch) => {
  return {
    fetchProjects: () => {
      dispatch(isLoading(true));
      setTimeout(() => {
        dispatch(getAllProjects());
      }, 300);
    }
  }
};

export const mapStateToProps = (state) => {
  const {allProjects, isLoading} = state;
  return {
    allProjects,
    isLoading
  }
};
