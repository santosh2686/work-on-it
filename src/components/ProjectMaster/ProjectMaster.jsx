import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux'
import ProjectItem from '../ProjectItem/ProjectItem.jsx';
import SideFilter from '../SideFilter/SideFilter.jsx';
import Pagination from '../Pagination/Pagination.jsx';
import Spinner from '../Spinner/Spinner.jsx';

import {mapStateToProps, mapDispatchToProps} from './projectMaster.conf';

class ProjectMaster extends Component {
  componentDidMount() {
    this.props.fetchProjects();
  }
  render() {
    const {allProjects, isLoading} = this.props;
    return(
      <div>
        <div className="flex flex-space-between align-center flex-no-shrink flex-responsive">
          <h4>Browse Projects</h4>
          <div className="breadcrumb">
            <ol>
              <li><NavLink to="/dashboard">Dashboard</NavLink></li>
              <li>Browse Projects</li>
            </ol>
          </div>
        </div>
        <div className="grid-row">
          <div className="grid-block cell-1of4 flex flex-vertical">
            <SideFilter/>
          </div>
          <div className="grid-block">
            <div className="bg-white pad-15 mar-t-20">

              <div className="flex bor-b-gray-light align-center pad-b-15">
                <span className="font-18 flex-1">All Projects</span>
                <div className="flex align-center">
                  <div className="mar-r-10">1,000 <span className="color-gray">Projects Found</span></div>
                  <div className="input-group pad-b-0 select-box relative">
                    <select className="input-control">
                      <option>Newest first</option>
                      <option>Lowest budget first</option>
                      <option>Highest budget first</option>
                      <option>Lowest bids</option>
                      <option>Highest bids</option>
                    </select>
                  </div>
                </div>
              </div>

              {isLoading && (<Spinner/>)}
              {!isLoading &&
                <div>
                  {allProjects.map((item, index) => {
                    return (<ProjectItem key={index} data={item}/>);
                  })}
                </div>
              }

              <Pagination/>

            </div>

          </div>
        </div>
      </div>
    );
  }
}

ProjectMaster.propTypes = {
  isLoading: PropTypes.bool,
  allProjects: PropTypes.array,
  fetchProjects: PropTypes.func
};
ProjectMaster.defaultProps = {
  isLoading: true,
  allProjects: [],
  fetchProjects: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectMaster);
