import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux'

import {mapStateToProps, mapDispatchToProps} from './dashBoard.conf';
import ProjectItem from '../ProjectItem/ProjectItem.jsx';
import Pagination from '../Pagination/Pagination.jsx';
import Spinner from '../Spinner/Spinner.jsx';

class DashBoard extends Component {
  componentDidMount() {
    this.props.fetchRecent();
  }
  render() {
    const {isLoading, projects, statistics} = this.props;
    if(isLoading) {
      return <Spinner/>
    }
    return (
      <div>
        <div className="grid-row">
          <div className="grid-block pad-b-20">
            <div className="bg-white pad-20 flex flex-space-between">
              <div className="dash-icon color-success">
                <i className="fa fa-group"/>
              </div>
              <div className="text-right">
                <p className="font-24">{statistics.buyer}</p>
                <span className="color-gray font-16">Total Buyers</span>
              </div>
            </div>
          </div>
          <div className="grid-block pad-b-20">
            <div className="bg-white pad-20 flex flex-space-between">
              <div className="dash-icon color-success">
                <i className="fa fa-group"/>
              </div>
              <div className="text-right">
                <p className="font-24">{statistics.seller}</p>
                <span className="color-gray font-16">Total Sellers123</span>
              </div>
            </div>
          </div>
          <div className="grid-block pad-b-20">
            <div className="bg-white pad-20 flex flex-space-between">
              <div className="dash-icon color-success">
                <i className="fa fa-thumbs-up"/>
              </div>
              <div className="text-right">
                <p className="font-24">{statistics.postings}</p>
                <span className="color-gray font-16">Daily Postings</span>
              </div>
            </div>
          </div>
          <div className="grid-block pad-b-20">
            <div className="bg-white pad-20 flex flex-space-between">
              <div className="dash-icon color-success">
                <i className="fa fa-thumbs-up"/>
              </div>
              <div className="text-right">
                <p className="font-24">{statistics.bids}</p>
                <span className="color-gray font-16">Daily Bids</span>
              </div>
            </div>
          </div>
        </div>
        <div className="bg-white pad-15">
          <div className="flex bor-b-gray-light align-center pad-b-15">
            <span className="font-18 flex-1">Recent Projects</span>
            <div className="flex align-center">
              <div className="mar-r-10">100 <span className="color-gray">Projects Found</span></div>
              <div className="input-group pad-b-0 select-box relative">
                <select className="input-control">
                  <option>Newest first</option>
                  <option>Lowest budget first</option>
                  <option>Highest budget first</option>
                  <option>Lowest bids</option>
                  <option>Highest bids</option>
                </select>
              </div>
            </div>
          </div>

          {projects.map((item, index) => {
            return (<ProjectItem key={index} data={item}/>);
          })}

          <Pagination/>

        </div>
      </div>);
  }
}

DashBoard.propTypes = {
  isLoading: PropTypes.bool,
  projects: PropTypes.array,
  statistics: PropTypes.object,
  fetchRecent: PropTypes.func
};

DashBoard.defaultProps = {
  isLoading: true,
  projects: [],
  statistics: {},
  fetchRecent: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(DashBoard);
