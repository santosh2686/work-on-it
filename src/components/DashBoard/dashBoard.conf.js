import { fetchProjects, isLoading } from '../../actions';

export const mapDispatchToProps = (dispatch) => {
  return {
    fetchRecent: () => {
      dispatch(isLoading(true));
      setTimeout(() => {
        dispatch(fetchProjects());
      }, 300);
    }
  }
};

export const mapStateToProps = (state) => {
  const {projects, statistics, isLoading} = state;
  return {
    projects,
    statistics,
    isLoading
  }
};
