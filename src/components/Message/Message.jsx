import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';

class Message extends Component {
  constructor(props) {
    super(props);
    this.closeHandler = this.closeHandler.bind(this);
  }

  closeHandler() {
    this.props.closeHandler();
  }

  render() {
    const { message, type, buttonText, listRoute } = this.props;
    return(
      <div className="alert-message bg-white text-center pad-30 mar-t-20">
        <div className={`icon ${type}`}>
          <span/>
        </div>
        <p className="pad-tb-20">
          {type === 'error' ? 'System not responding, please try after some time' : message}
        </p>
        <div>
          <button className={`btn btn-${type === "error" ? "primary" : "default"}`} onClick={this.closeHandler}>
            Close
          </button>
          {type === "success" && <NavLink to={listRoute} className="btn btn-primary mar-l-5">
            {buttonText}
          </NavLink>}
        </div>
      </div>
    );
  }
}

Message.prototypes = {
  message: PropTypes.string,
  type: PropTypes.string,
  buttonText: PropTypes.string,
  listRoute: PropTypes.string
};

Message.defaultProps = {
  message: '',
  type: '',
  buttonText: '',
  listRoute: ''
};

export default Message;