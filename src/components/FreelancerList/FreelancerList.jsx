import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import Freelancer from '../Freelancer/Freelancer.jsx';
import {mapStateToProps, mapDispatchToProps} from './freelancerList.conf';

class FreelancerList extends Component {
  componentDidMount() {
    this.props.fetchList();
  }
  render() {
    const {freelancerList} = this.props;

    return (<div>
      {freelancerList.map((item, index) => {
        return (<Freelancer key={index} data={item}/>);
      })}
    </div>)
  }
}

FreelancerList.propTypes = {
  freelancerList: PropTypes.array,
  id: PropTypes.string,
  fetchList: PropTypes.func
};

FreelancerList.defaultProps = {
  freelancerList: [],
  id: '',
  fetchList: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(FreelancerList);
