import { fetchFreelancerList } from '../../actions';

export const mapDispatchToProps = (dispatch, {id}) => {
  return {
    fetchList: () => {
      dispatch(fetchFreelancerList(id));
    }
  }
};

export const mapStateToProps = (state) => {
  const { freelancerList } = state;
  return {
    freelancerList
  }
};
