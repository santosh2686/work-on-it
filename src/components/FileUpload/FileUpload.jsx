import React, {Component} from 'react';

class FileUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
    this.selectFile = this.selectFile.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
  }
  selectFile() {
   this.refs.fileControl.click();
  }
  changeHandler(e) {
    this.setState({value: e.target.value});
  }
  render(){
    return(<div className="relative file-upload flex pad-l-10" onClick={this.selectFile}>
      <input ref="fileControl" className="file-control hide" type="file" value={this.state.value} onChange={this.changeHandler} />
      <div className="flex align-center flex-1 text-ellipsis bor-t-gray-light bor-l-gray-light bor-b-gray-light pad-l-10">{this.state.value}</div>
      <button className="btn btn-primary mar-l-0 relative overflow-hidden">
        <i className="fa fa-upload absolute"/>
        <span className="show-block relative">Select File</span>
      </button>
    </div>)
  }
}

export default FileUpload;