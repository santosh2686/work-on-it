import React from 'react';
import PropTypes from 'prop-types';

const Spinner = ({
  size,
  type
}) => {
  const spinnerClasses = `spinner center ${size} ${type}`;
  return (
    <div className="bg-white pad-30 mar-t-20">
      <div className={spinnerClasses}/>
    </div>
  );
};

Spinner.propTypes = {
  size: PropTypes.string,
  type: PropTypes.string
};

Spinner.DefaultProps = {
  size: '',
  type: ''
};

export default Spinner;