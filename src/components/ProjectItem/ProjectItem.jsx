import React from 'react';
import {NavLink} from 'react-router-dom';
import TaskInfo from '../TaskInfo/TaskInfo.jsx';
import Chip from '../Chip/Chip.jsx';
import { daysLeft } from '../../utils';

const ProjectItem = ({
  data
}) => {
  return (
    <div className="bor-b-gray-light pad-tb-15 project-item">
      <div className="grid-row">
        <div className="grid-block pad-b-20">
          <div className="pad-b-5">
            <NavLink to={`/project/${data.id}`} className="font-18">{data.title}</NavLink>
            <span className="font-12 show-inline-block bg-warning pad-lr-10 mar-l-10 color-white days-left">
              {daysLeft(data.date, data.lastDate)} Days Left
            </span>
          </div>
          <p className="text-justify pad-r-30">{data.description}</p>
        </div>
        <div className="grid-block cell-1of3 pad-b-20">
          <TaskInfo data={data} />
        </div>
      </div>
      <div className="flex">
        <div className="flex-1 flex flex-wrap">
          {data.skills.map((skill, index) => {
            return (<Chip key={index} data={skill}/>);
          })}
        </div>
        <div className="text-right">
          <NavLink to={`/project/${data.id}`} className="btn btn-primary bid-now">Bid Now</NavLink>
        </div>
      </div>
    </div>
  )
};

export default ProjectItem;
