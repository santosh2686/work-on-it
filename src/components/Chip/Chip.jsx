import React, {Component} from 'react';
import PropTypes from 'prop-types';

const Chip = ({
  data
}) => {
  return (
    <div className="chip bg-success color-white pad-tb-5 pad-lr-10 mar-5 text-ellipsis">
      {data}
    </div>
  );
};

Chip.propTypes = {
  data: PropTypes.string
};

Chip.defaultProps = {
  data: ''
};

export default Chip;