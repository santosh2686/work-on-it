import React from  'react';

const Freelancer = ({
  data
}) => {
  const rating = [];
  for(let i = 0; i < 5; i++) {
    if(i < data.rating) {
      rating.push(<i key={i} className="fa fa-star mar-r-5 color-warning"/>);
    } else {
      rating.push(<i key={i} className="fa fa-star mar-r-5 color-gray-light"/>);
    }
  }
  return (<div className="bor-b-gray-light pad-tb-10">
    <div className="grid-row">
      <div className="grid-block flex align-center">
        <div className="user-profile">
          <i className="fa fa-user-circle"/>
        </div>
        <div className="flex-1 pad-l-20">
          <span className="font-18">{data.name}</span>
          <span className="font-16 mar-l-15">
            {rating}
          </span>
          <p className="text-justify pad-r-30 color-gray">{data.comment}</p>
        </div>
      </div>
      <div className="grid-block cell-1of5">
        <div className="bg-gray-lighter text-center pad-15">
          <span className="font-18">${data.bid}</span>
          <span className="show-block color-gray">in {data.duration}</span>
        </div>
      </div>
    </div>
  </div>)
};

export default Freelancer;
