import React from 'react';
import Panel from '../Panel/Panel.jsx';

const SideFilter = () => {
  return (
    <div className="flex flex-vertical flex-1">
      <Panel title="Search Keyword">
        <div className="input-group pad-b-0">
          <input type="text" className="input-control" placeholder="Search"/>
        </div>
      </Panel>
      <Panel title="Filter By" extraClasses="flex-1">
        <div className="pad-b-20">
          <h5 className="mar-b-10 font-bold">Posted</h5>
          <div>
            <input type="checkbox" id="today" name="today"/>
            <label className="mar-l-10" htmlFor="today">Today</label>
          </div>
          <div>
            <input type="checkbox" id="lastWeek" name="lastWeek"/>
            <label className="mar-l-10" htmlFor="lastWeek">Last Week</label>
          </div>
          <div>
            <input type="checkbox" id="lastMonth" name="lastMonth"/>
            <label className="mar-l-10" htmlFor="lastMonth">Last Month</label>
          </div>
        </div>

        <div className="pad-b-20">
          <h5 className="mar-b-10 font-bold">Type</h5>
          <div>
            <input type="checkbox" id="fixed" name="fixed"/>
            <label className="mar-l-10" htmlFor="fixed">Fixed Price</label>
          </div>
          <div>
            <input type="checkbox" id="hourly" name="hourly"/>
            <label className="mar-l-10" htmlFor="hourly">Hourly Rate</label>
          </div>
        </div>

        <div className="pad-b-20">
          <h5 className="mar-b-10 font-bold">Budget</h5>
          <div className="flex align-center">
            <div className="input-group pad-b-0">
              <input type="number" className="input-control" placeholder="Min"/>
            </div>
            <div className="pad-lr-10">to</div>
            <div className="input-group pad-b-0">
              <input type="number" className="input-control" placeholder="Max"/>
            </div>
          </div>
        </div>

        <div>
          <h5 className="mar-b-10 font-bold">Duration</h5>
          <div className="input-group pad-b-0 select-box relative">
            <select className="input-control">
              <option>Select Duration</option>
              <option>Less than a week</option>
              <option> 1 week to 4 week</option>
              <option>1 month to 4 month</option>
              <option>Over 6 months</option>
              <option>Unspecified</option>
            </select>
          </div>
        </div>
      </Panel>
    </div>);
};

export default SideFilter;