import React from 'react';

const TaskInfo = ({
  data
}) => {
  return(
    <div>
      <div className="pad-b-10 no-wrap text-right">
        Posted on: <span className="color-gray">
        {new Date(data.date).toLocaleString()}
      </span>
      </div>
      <ul className="bg-gray-lighter flex pad-10 text-center task-info no-wrap">
        <li className="pad-lr-10 bor-r-gray-light flex-1">
          <span className="show-block font-18">{data.noOfBids}</span>
          <span className="color-gray">Bids</span>
        </li>
        <li className="pad-lr-10 bor-r-gray-light flex-1">
          <span className="show-block font-18">${data.lowestBid}</span>
          <span className="color-gray">Lowest Bid</span>
        </li>
        <li className="pad-lr-10 flex-1">
          <span className="show-block font-18">${data.minRate} - ${data.maxRate}</span>
          <span className="color-gray"> {data.type} Rate</span>
        </li>
      </ul>
      <div className="pad-t-10 no-wrap text-right">
        Last day for Bid: <span className="color-gray">{new Date(data.lastDate).toLocaleString()}</span>
      </div>
    </div>
  )
};

export default TaskInfo;
