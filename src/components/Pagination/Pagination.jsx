import React from 'react';

const Pagination = () => {
  return(
    <div className="flex pad-t-15">
      <div className="flex-1 flex align-center">
        <span className="mar-r-10">Rows Per Page:</span>
        <div className="input-group pad-b-0 select-box relative mini">
          <select className="input-control mini">
            <option>10</option>
            <option>20</option>
            <option>50</option>
            <option>100</option>
          </select>
        </div>
      </div>
      <div className="text-right">
         <span className="btn btn-success disabled">Prev</span>
          <span className="mar-l-15 mar-r-20">1 of 10</span>
         <span className="btn btn-success">Next</span>
      </div>
    </div>
  )
};

export default Pagination;