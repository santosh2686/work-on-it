import React from 'react';
import {NavLink} from 'react-router-dom';
import Panel from '../Panel/Panel.jsx';
import FileUpload from '../FileUpload/FileUpload.jsx';

const PostProject = () => {
  return(<div>
    <div className="flex flex-space-between align-center flex-no-shrink flex-responsive">
      <h4>Post a New Project</h4>
      <div className="breadcrumb">
        <ol>
          <li><NavLink to="/dashboard">Dashboard</NavLink></li>
          <li>Post a New Project</li>
        </ol>
      </div>
    </div>
      <Panel title="Project Detail" isRequired>
        <div className="grid-row">
          <div className="grid-block input-group pad-b-20">
            <label className="input-label">Project Title<span className="color-danger">*</span></label>
            <input type="text" className="input-control"/>
          </div>
          <div className="grid-block input-group pad-b-20">
            <label className="input-label pad-l-30">How do you want to pay?<span className="color-danger">*</span></label>
            <div className="pad-t-10  pad-l-30">
            <span className="mar-r-20">
              <input type="radio" name="radio" id="fixed" checked/>
              <label htmlFor="fixed" className="mar-l-10">Fixed Rate</label>
            </span>
            <span>
              <input type="radio" name="radio" id="hourly"/>
              <label htmlFor="hourly" className="mar-l-10">Hourly Rate</label>
            </span>
            </div>
          </div>
          <div className="grid-block input-group pad-b-20">
            <label className="input-label">Min Budget<span className="color-danger">*</span></label>
            <input type="text" className="input-control"/>
          </div>
          <div className="grid-block input-group pad-b-20">
            <label className="input-label">Max Budget<span className="color-danger">*</span></label>
            <input type="text" className="input-control"/>
          </div>
        </div>
        <div className="grid-row">
          <div className="grid-block input-group">
            <label className="input-label">Skills Required</label>
            <input type="text" className="input-control"/>
          </div>
          <div className="grid-block input-group">
            <label className="input-label">Attachments</label>
           <FileUpload/>
          </div>
          <div className="grid-block input-group">
            <label className="input-label">Last date for Bidding</label>
            <input type="date" className="input-control"/>
          </div>
        </div>
        <div className="grid-row">
          <div className="grid-block input-group pad-b-0">
            <label className="input-label">Project Description<span className="color-danger">*</span></label>
            <textarea className="input-control"/>
          </div>
        </div>
      </Panel>
      <div className="pad-tb-15 text-right">
        <NavLink to="/dashboard" className="btn btn-default">Cancel</NavLink>
        <button type="button" className="btn btn-primary">Post a Project</button>
      </div>
  </div>);
};

export default PostProject;
