import axios from 'axios';

export const IS_LOADING = 'IS_LOADING';
export const RECENT_PROJECTS = 'RECENT_PROJECTS';
export const ALL_PROJECTS = 'ALL_PROJECTS';
export const STATISTICS = 'STATISTICS';
export const SELECTED_PROJECT = 'SELECTED_PROJECT';
export const FREELANCER_LIST = 'FREELANCER_LIST';

export function isLoading(flag) {
  return {
    type: IS_LOADING,
    flag
  }
}

export function recentProjects(res) {
  return {
    type: RECENT_PROJECTS,
    res
  }
}

export function statistics(res) {
  return {
    type: STATISTICS,
    res
  }
}

export function allProject(res) {
  return {
    type: ALL_PROJECTS,
    res
  }
}


export function selectedProject(id, res) {
  return {
    type: SELECTED_PROJECT,
    id,
    res
  }
}

export function freelancerList(res) {
  return {
    type: FREELANCER_LIST,
    res
  }
}

function getProjects() {
  return axios.get('/mock/project.json');
}

function getStatistics() {
  return axios.get('/mock/statistics.json');
}

export function fetchProjects() {
  return (dispatch) => {
    axios.all([getProjects(), getStatistics()]).then(axios.spread(function (projects, stat) {
        dispatch(recentProjects(projects.data));
        dispatch(statistics(stat.data));
        dispatch(isLoading(false));
    }));
  }
}

export function getAllProjects() {
  return (dispatch, getState) => {
    axios.get('/mock/project.json').then(res => {
      dispatch(allProject(res.data));
      dispatch(isLoading(false));
    });
  }
}

export function getProjectById(id) {
  return (dispatch, getState) => {
    const state = getState();
    if(state.selectedProject[id]) {
      dispatch(isLoading(false));
      return;
    }
    axios.get(`/mock/${id}.json`).then(res => {
      dispatch(selectedProject(id, res.data));
      dispatch(isLoading(false));
    });
  }
}

export function fetchFreelancerList(id) {
  return (dispatch) => {
    axios.get(`/mock/${id}Bids.json`).then(res => {
      dispatch(freelancerList(res.data));
    });
  }
}




