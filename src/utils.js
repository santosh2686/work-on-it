export const daysLeft = (startDate, endDate) => {
  const date1 = new Date();
  const date2 = new Date(endDate);
  const timeDiff = Math.abs(date2.getTime() - date1.getTime());
  return Math.ceil(timeDiff / (1000 * 3600 * 24));
};
