const path = require('path');
const webpack = require('webpack');
const apiEndPoint = 'https://api.mongolab.com';

module.exports = {
  entry: './index.js',
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'bundle.js',
    publicPath: '/build/'
  },
  devServer: {
    port: process.env.PORT || 9090,
    host: '0.0.0.0',
    historyApiFallback: true,
    disableHostCheck: true,
    overlay: true,
    compress: true
  },
  stats: {
    colors: true,
    modules: true,
    assets: true,
    children: true,
    chunks: true,
    depth: true,
    entrypoints: true,
    providedExports: true,
    usedExports: true,
    timings: true,
    hash: true,
    maxModules: 0,
    errors: true,
    warnings: true
  },
  resolve: {
    alias: {
      'common': path.resolve(__dirname, './src/components/common')
    }
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }, {
      test: /\.s?css$/,
      use: ['style-loader', 'css-loader', 'sass-loader']
    }, {
      test: /\.(png|svg|jpg|gif)$/,
      loader: 'file-loader',
      query: {
        name: 'images/[name].[ext]?[hash]'
      }
    }]
  }
};
