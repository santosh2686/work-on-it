import React from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Route, Switch, Redirect, NavLink} from "react-router-dom";

import {configureStore} from './configureStore';
import DashBoard from './src/components/DashBoard/DashBoard.jsx';
import ProjectMaster from './src/components/ProjectMaster/ProjectMaster.jsx';
import Project from './src/components/Project/Project.jsx';
import PostProject from './src/components/PostProject/PostProject.jsx';
const store = configureStore();

const App = () => (
  <Provider key="store" store={store}>
    <Router>
      <div>
        <div className="pad-tb-10 header">
          <div className="center container flex align-center flex-space-between">
            <div className="font-24 font-bold color-white">WorkZ</div>
            <div>
              <NavLink to="/post-project" className="btn btn-primary">Post Project</NavLink>
            </div>
          </div>
        </div>
        <div className="bg-white bor-b-gray-light">
          <div className="center container">
            <ul className="flex navigation">
              <li className="text-center bor-r-gray-light bor-l-gray-light">
                <NavLink to="/dashboard" className="color-gray-dark show-block pad-tb-10 pad-lr-20">
                  <i className="fa fa-dashboard"/>
                  <span className="show-block">DashBoard</span>
                </NavLink>
              </li>
              <li className="text-center bor-r-gray-light">
                <NavLink to="/projects" className="color-gray-dark show-block pad-tb-10 pad-lr-20">
                  <i className="fa fa-list"/>
                  <span className="show-block">Browse Projects</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
        <div className="center container pad-tb-20">
          <Switch>
            <Route exact path='/' render={() => <Redirect to="/dashboard"/>}/>
            <Route exact path="/dashboard" component={DashBoard}/>
            <Route exact path="/projects" component={ProjectMaster}/>
            <Route exact path="/post-project" component={PostProject}/>
            <Route path="/project/:id" component={Project}/>
            <Route render={() => <Redirect to="/dashboard"/>}/>
          </Switch>
        </div>
      </div>
    </Router>
  </Provider>
);

export default (App)
